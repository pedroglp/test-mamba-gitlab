import Home from './routes/Home.html';
import AddCostumer from './routes/AddCostumer.html';
import CheckCostumer from './routes/CheckCostumer.html';

export default {
  '/': Home,
  '/add-costumer': AddCostumer,
  '/check-costumer': CheckCostumer,
};
