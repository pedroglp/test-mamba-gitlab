import MambaStore from '@mamba/store';
import Storage from '@mamba/pos/api/storage.js';

export const INITIAL_DATA = {
  costumers: Storage.getItem('costumers') || [],
};

const store = MambaStore(INITIAL_DATA);

if (__DEV__) {
  window.MambaStore = store;
}

export default store;
